## Problem statement

> What is the issue being faced and needs addressing?

## Who will benefit?

> Will this fix a problem that only one user has, or will it benefit a lot of people?

## Benefits

> What could you do with this feature that you can not do now? How would it improve your workflow?

## Suggested solution

> How does this feature could be implemented in Packmind?

## Examples

> Are there any examples of this which exist in other software?

## Priority

- [ ] High (This will bring a huge increase in performance/productivity/usability/legislative cover)
- [ ] Medium (This will bring a good increase in performance/productivity/usability)
- [ ] Low (anything else e.g., trivial, minor improvements)