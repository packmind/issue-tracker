## Description
- **Summary:** *A short description of the issue*
- **Steps to reproduce:** *How to reproduce this issue?*
- **Actual result:** *What is the observed behavior?*
- **Expected result:** *What is the expected behavior?*

## Context
- **Application**: *Web application / IDE extension (VSCode, Jetbrains...) / Browser extension*
- **Application version**: *if the issue is related to an extension*
- **OS**: *Windows/MacOS/Linux*
- **Browser**: *Chrome/Firefox/... + version*
- **URL**: *URL of the page where the issue has been found*

## Relevant logs and/or screenshots

> Please join every screenshots / videos / console output that may help us identify and fix the issue