# Packmind - Issue Tracker

> *Amplify < knowledge > improve [engineering performance]*

**Website**: https://www.packmind.com/


### What can you do from here?
- [Report an issue](https://gitlab.com/packmind/issue-tracker/-/issues/new?issuable_template=Default): let our team know whenever you meet a **bug** or an **expected behavior** and follow its resolution progress

- [Suggest a new feature/improvement](https://gitlab.com/packmind/issue-tracker/-/issues/new?issuable_template=Feature%20proposal): help us shape Packmind by telling us what new features or improvements you would love to see